"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # I know that table driven logic would be a simpler implementation,
    # but sometimes the simplest logic is the hardest to implement.

    # Retrospectively this would've been much easier with loops and abstraction,
    # but I really just need the functionality
    km = control_dist_km
    brevet = brevet_dist_km
    start = arrow.get(brevet_start_time, 'YYYY-MM-DDTHH:mm:ss')

    print(km, brevet, start)

    if km < 200 and brevet == 200:
        hours, minutes, seconds = hours_and_mins(km, 34)
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 200 <= km <= 220 and brevet == 200:
        hours, minutes, seconds = hours_and_mins(200, 34)
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 220 < km and brevet == 200:
        open_time = "Distance too long for brevet"

    elif 300 > km and brevet == 300:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins((km-200), 32)
        hours += hour1
        minutes += minute1
        seconds += second1
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 300 <= km <= 320 and brevet == 300:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins(100, 32)
        hours += hour1
        minutes += minute1
        seconds += second1
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 330 < km and brevet == 300:
        return "Distance too long for brevet"

    elif km < 400 and brevet == 400:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins((km-200), 32)
        hours += hour1
        minutes += minute1
        seconds += second1
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 400 <= km <= 440 and brevet == 400:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins(200, 32)
        hours += hour1
        minutes += minute1
        seconds += second1
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 400 < km and brevet == 400:
        return "Distance too long for brevet"

    elif km < 600 and brevet == 600:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins(200, 32)
        hour2, minute2, second2 = hours_and_mins((km-400), 30)
        hours += hour1 + hour2
        minutes += minute1 + minute2
        seconds += second1 + second2
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 600 <= km <= 660 and brevet == 600:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins(200, 32)
        hour2, minute2, second2 = hours_and_mins(200, 30)
        hours += hour1 + hour2
        minutes += minute1 + minute2
        seconds += second1 + second2
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 660 < km and brevet == 600:
        return "Distance too long for brevet"

    elif 1000 > km and brevet == 1000:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins(200, 32)
        hour2, minute2, second2 = hours_and_mins(200, 30)
        hour3, minute3, second3 = hours_and_mins((km - 600), 28)
        hours += hour1 + hour2 + hour3
        minutes += minute1 + minute2 + minute3
        seconds += second1 + second2 + minute3
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 1000 < km < 1100 and brevet == 1000:
        hours, minutes, seconds = hours_and_mins(200, 34)
        hour1, minute1, second1 = hours_and_mins(200, 32)
        hour2, minute2, second2 = hours_and_mins(200, 30)
        hour3, minute3, second3 = hours_and_mins(400, 28)
        hours += hour1 + hour2 + hour3
        minutes += minute1 + minute2 + minute3
        seconds += second1 + second2 + minute3
        open_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 1100 < km and brevet == "1000":
        return "Distance too long for brevet"

    else:
        return"Unexpected Parse Error"

    return open_time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # I know this is unnecessary but its nice to have simpler variable names and I
    # didnt want to change the original names so that it wouldnt mess up any bot testing
    km = control_dist_km
    brevet = brevet_dist_km
    start = arrow.get(brevet_start_time, 'YYYY-MM-DDTHH:mm:ss')

    print(km, brevet, start)
    if km < 200 and brevet == 200:
        hours, minutes, seconds = hours_and_mins(km, 15)
        close_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 200 <= km <= 220 and brevet == 200:
        close_time = start.shift(hours=+ 13.5)

    elif km > 220 and brevet == 200:
        return "Distance too long for brevet"

    elif km > 300 and brevet == 300:
        hours, minutes, seconds = hours_and_mins(km, 15)
        close_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 300 <= km <= 320 and brevet == 300:
        close_time = start.shift(hours=+ 20)

    elif km < 330 and brevet == 300:
        return"Distance too long for brevet"

    elif km < 400 and brevet == 400:
        hours, minutes, seconds = hours_and_mins(km, 15)
        close_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 400 <= km <= 440 and brevet == 400:
        close_time = start.shift(hours=+ 27)

    elif km > 440 and brevet == 400:
        return "Distance too long for brevet"

    elif km < 600 and brevet == 600:
        hours, minutes, seconds = hours_and_mins(km, 15)
        close_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 600 <= km <= 660 and brevet == 600:
        close_time = start.shift(hours=+ 40)

    elif km > 660 and brevet == 600:
        return "Distance too long for brevet"

    elif 1000 > km and brevet == 1000:
        hours, minutes, seconds = hours_and_mins(600, 15)
        hour1, minute1, second1 = hours_and_mins((km-600), 11.428)
        hours += hour1
        minutes += minute1
        seconds += second1
        close_time = start.shift(hours=+hours, minutes=+minutes, seconds=seconds)

    elif 1000 < km < 1100 and brevet == 1000:
        close_time = start.shift(hours=+ 75)

    elif 1100 < km and brevet == 1000:
        return "Distance too long for brevet"

    else:
        return "Unexpected Parse Error"

    return close_time.isoformat()

    # Use for reference later original return format
    # return arrow.now().isoformat()


def hours_and_mins(km, speed):
    '''
    Auxillary function to determine leftover time from hours
    :param km: Input distance in kilometers
    :param speed: input speed in km/hour
    :return: a triple of the hours. minutes, and seconds in whole numbers
    '''

    time = km/speed

    hours = int(time)
    minutes = (time * 60) % 60
    seconds = (time * 3600) % 60

    return hours, minutes, seconds
