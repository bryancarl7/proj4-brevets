"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import request
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config

import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    
    # Unfortunately I wasnt able to get full functionality in time. Initially I had passed all the varialbles
    # through Json and these values were used, but I could not for the life of me get the javascript to load the actual
    # damn value. I spent a good 5 hours trying to get it to display. All the correct info was being passed in the Jqueries
    # I even used the debugger to make sure they were actually being passed and the JS was catching it. 
    # It just wouldnt physically display them.
    
    # On top of all else something is wrong with my docker and it wont even display the html anymore, giving me a 
    # "server not found error" and at this point its too late (which is entirely on me), making it impossible to debug
    
    # So I filled out my test suites and updated the readme and I hope that I can recover some partial credit for this project
    
    #date = request.args.get('date', 0, type=str)
    #time = request.args.get('time', 0, type=str)
    #start_time = arrow.get(date + " " + time, "ddd MM/DD/YYYY HH:mm")

    # just kept them at 200 because parsing the values was more complications on top of what I was trying to debug
    open_time = acp_times.open_time(km, 200, arrow.now().isoformat())
    close_time = acp_times.close_time(km, 200, arrow.now().isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
