'''
Nose test suite for acp_times.py
At least 5 tests to confirm that acp_times.py has
full functionality as intended by the ACP times calculator
'''

import nose
from acp_times import open_time, close_time


def test_open_time():
    assert(open_time(132, 200, '2018-11-06T17:26:21.076966-08:00') == '2018-11-06T21:20:13.941176+00:00')
    assert(open_time(600, 600, '2018-11-06T17:26:21.076966-08:00') == '2018-11-07T12:15:13.941176+00:00')
    assert(open_time(1200, 1000, '2018-11-06T17:26:21.076966-08:00') == 'Unexpected Parse Error')


def test_close_time():
    assert(close_time(175, 200, '2018-11-06T17:26:21.076966-08:00') == '2018-11-07T05:06:21+00:00')
    assert(close_time(400, 600, '2018-11-06T17:26:21.076966-08:00') == '2018-11-07T20:06:21+00:00')
    assert(close_time(700, 1000, '2018-11-06T17:26:21.076966-08:00') == '2018-11-08T18:11:24.150158+00:00')
