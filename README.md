# Project 4: Brevet time calculator with Ajax

Reimplement the RUSA ACP controle time calculator with flask and ajax.

Credits to Michal Young and Ram Durairajan for the initial version of this code.

Updated/modified by University of Oregon Student: Bryan Carl

# ACP Control Times

The initial calculator can be found here: https://rusa.org/pages/acp-brevet-control-times-calculator

The rules are simple: if it is within a given speen bracket you must calculate their speed according to that particular speed bracket and when it happened.

# Running it

To run this in terminal simply clone all files and within your terminal in your cd'd clone folder type:

docker build -t sample_flask .

After that:

docker run -d -p 5000:5000 sample_flask

And you should be prompted with an ACP times calculator!

If there are issues you may contact me at bcarl@uoregon.edu

# Credits

I would like to thank my mom, she's alwasy super proud of me and hopes the best.